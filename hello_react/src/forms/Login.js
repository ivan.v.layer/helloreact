import React, { useEffect, useState } from "react";
import PropTypes from 'prop-types';
import './Login.css';
import keypict from './Login.jpg';

async function loginUser(credentials) {
    return fetch('http://localhost:5001/api/LoginApi',
        {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(credentials)
        })
        .then(data => data.json())

}

export default function Login({setToken}) {
    const [username, setUserName] = useState();
    const [password, setPassword] = useState();

    const handleSubmit = async e => {
        e.preventDefault();
        const token = await loginUser({ username, password });
        
        if(token.status == Response.HTTP_OK)
            setToken(token.token);
    }

return(
        <div className="login-css">
        <img src={keypict} height={100} width={200}/>
        <h1>Please login (password = 123)</h1>
        <form onSubmit={handleSubmit}>
            <label>
                <p>Login:</p>
                <input type="text" onChange={e => setUserName(e.target.value)}/>
            </label>
            <label>
                <p>Password:</p>
                <input type="password" onChange={e => setPassword(e.target.value)} />
            </label>
            <div>
                <button type="submit">Submit</button>
            </div>
        </form>
     </div>
)

}

Login.propTypes = {
    setToken: PropTypes.func.isRequired
}