﻿namespace server_api.DataModel
{
    public class LoginData
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
