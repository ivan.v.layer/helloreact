﻿using Microsoft.AspNetCore.Mvc;
using server_api.DataModel;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace server_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginApiController : ControllerBase
    {
        ILogger<LoginApiController> _logger;
        
        public LoginApiController(ILogger<LoginApiController> logger)
        {
            _logger = logger;
        }

        // POST api/<LoginApi>
        [HttpPost]
        public IActionResult login([FromBody] LoginData credentials)
        {
            _logger.LogInformation($"Username login attempt: {credentials.Username}");

            if (credentials.Password == "123")
                return Ok("{\"token\":\"usertoken\"}");
            else
                return BadRequest();
        }

  
    }
}
